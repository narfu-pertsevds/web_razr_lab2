<?php

declare(strict_types=1);
require_once("App/autoload.php");
require_once("vendor/autoload.php");

use PHPUnit\Framework\TestCase;
use App\Controller\Box;
use App\Controller\Plane;

final class TestPlane extends TestCase
{
    public function testDeliveryToMoscowMustSucceed(): void
    {
        $box = new Box("Конфеты", 1000);
        $plane = new Plane(26270);
        $res = $plane->canDeliver($box, "Москва");
        $this->assertSame($res, true);
    }
    
    public function testDeliveryToUSAMustSucceed(): void
    {
        $box = new Box("Конфеты", 1000);
        $plane = new Plane(26270);
        $res = $plane->canDeliver($box, "США");
        $this->assertSame($res, true);
    }

    public function testDeliveryToGermanyMustSucceed(): void
    {
        $box = new Box("Конфеты", 1000);
        $train = new Plane(26270);
        $res = $train->canDeliver($box, "Germany");
        $this->assertSame($res, true);
    }

    public function testDeliveryOverweightMustFail(): void
    {
        $box = new Box("Конфеты", 30000);
        $plane = new Plane(26270);
        $res = $plane->canDeliver($box, "Москва");
        $this->assertSame($res, false);
    }
}
