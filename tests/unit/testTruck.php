<?php

declare(strict_types=1);
require_once("App/autoload.php");
require_once("vendor/autoload.php");

use PHPUnit\Framework\TestCase;
use App\Controller\Box;
use App\Controller\Truck;

final class TestTruck extends TestCase
{
    public function testDeliveryToMoscowMustSucceed(): void
    {
        $box = new Box("Конфеты", 1000);
        $truck = new Truck(26270);
        $res = $truck->canDeliver($box, "Москва");
        $this->assertSame($res, true);
    }
    public function testDeliveryToUSAMustFail(): void
    {
        $box = new Box("Конфеты", 1000);
        $truck = new Truck(26270);
        $res = $truck->canDeliver($box, "США");
        $this->assertSame($res, false);
    }

    public function testDeliveryToGermanyMustSucceed(): void
    {
        $box = new Box("Конфеты", 1000);
        $truck = new Truck(26270);
        $res = $truck->canDeliver($box, "Germany");
        $this->assertSame($res, true);
    }

    public function testDeliveryOverweightMustFail(): void
    {
        $box = new Box("Конфеты", 30000);
        $truck = new Truck(26270);
        $res = $truck->canDeliver($box, "Москва");
        $this->assertSame($res, false);
    }
}
