<?php

namespace App\Controller;

class Truck extends Transport
{
    function canDeliver(Box $box, string $dest)
    {
        if ($dest === "США")
            return false;
        return parent::canDeliver($box, $dest);
    }
}
