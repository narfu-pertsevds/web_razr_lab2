<?php

namespace App\Controller;

class Box
{
    public function __construct(public string $type, public int $weight)
    {
    }
}
