<?php

namespace App\Controller;

use \App\Interface\CanDeliver;

class Transport implements CanDeliver
{
    public function __construct(public int $weight_limit)
    {
    }

    function canDeliver(Box $box, string $dest)
    {
        if ($box->weight > $this->weight_limit)
            return false;

        return true;
    }
}
