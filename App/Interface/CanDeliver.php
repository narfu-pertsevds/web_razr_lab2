<?php
namespace App\Interface;

use \App\Controller\Box;

interface CanDeliver
{
    public function canDeliver(Box $box, string $dest);
}
